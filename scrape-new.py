import scrapy
from scrapy.crawler import CrawlerProcess
import os
import pandas as pd

# scrape the new data only


class forexSpider(scrapy.Spider):
    name = "forex_spider"
    allowed_domains = ["bankofcanada.ca"]
    start_urls = [
        "https://www.bankofcanada.ca/rates/exchange/daily-exchange-rates/"]

    def parse(self, response):

        SET1_SELECTOR = ".bocss-table__thead"

        for name in response.css(SET1_SELECTOR):
            TITLE_SELECTOR = "th ::text"
            header = name.css(TITLE_SELECTOR).extract()

            SET_SELECTOR = ".bocss-table__tr"

            for currency in response.css(SET_SELECTOR):

                RATE_SELECTOR = "td ::text"
                yield {
                   # header[1]: currency.css(RATE_SELECTOR)[0].extract(),
                    header[2]: currency.css(RATE_SELECTOR)[1].extract(),
                    header[3]: currency.css(RATE_SELECTOR)[2].extract(),
                    header[4]: currency.css(RATE_SELECTOR)[3].extract(),
                    header[5]: currency.css(RATE_SELECTOR)[4].extract()}


print("Updating data, one moment please ... ")
process = CrawlerProcess({"FEED_FORMAT": "csv", "FEED_URI": "rates1.csv", "LOG_ENABLED": False})
process.crawl(forexSpider)
process.start()

print("*************** Finished updating data ***************")

data = pd.read_csv("rates.csv", header=0)
new_data = pd.read_csv("rates1.csv", header=0)

# add the new data to the master list of data and update rates.csv
updated_data = pd.concat([data, new_data], axis=1)


# remove uncessary columns with `Unnamed` in title
to_drop = updated_data.columns.drop(
    list(updated_data.filter(regex="Unnamed:")))
updated_data = updated_data[to_drop]

# # write results to csv
updated_data.to_csv("rates.csv", float_format="%.6f")

# # remove the daily data
os.remove("rates1.csv")
