# Forex Scraper

A scraper that will scrape the daily exchange rates from the Bank of Canada webpage and output into a `.csv` file.

## Usage
To get the table of currently available rates, run: 
```
python3 scrape-all.py
``` 

To update the `rates.csv` file with the latest rates run: 

```
python3 scrape-new.py
```

One can also automate the scraping to every weekday using `crontab` on OSX.