import scrapy
from scrapy.crawler import CrawlerProcess


class forexSpider(scrapy.Spider):
    name = "forex_spider"

    allowed_domains = ["bankofcanada.ca"]
    start_urls = ["https://www.bankofcanada.ca/rates/exchange/daily-exchange-rates/"]

    def parse(self, response):

        SET1_SELECTOR = ".bocss-table__thead"

        for name in response.css(SET1_SELECTOR):
            TITLE_SELECTOR = "th ::text"
            header = name.css(TITLE_SELECTOR).extract()

            SET_SELECTOR = ".bocss-table__tr"

            for currency in response.css(SET_SELECTOR):

                NAME_SELECTOR = "th ::text"
                RATE_SELECTOR = "td ::text"
                yield {
                    "Currency": currency.css(NAME_SELECTOR).extract_first(),
                    header[1]: currency.css(RATE_SELECTOR)[0].extract(),
                    header[2]: currency.css(RATE_SELECTOR)[1].extract(),
                    header[3]: currency.css(RATE_SELECTOR)[2].extract(),
                    header[4]: currency.css(RATE_SELECTOR)[3].extract(),
                    header[5]: currency.css(RATE_SELECTOR)[4].extract(),
                }

print("Downloading data, one moment please ... ")

process = CrawlerProcess({"FEED_FORMAT": "csv", "FEED_URI": "rates.csv", "LOG_ENABLED": False})
process.crawl(forexSpider)
process.start()

print("*************** Finished Scraping ***************")
